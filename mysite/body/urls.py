from rest_framework.routers import DefaultRouter

from body.viewset import FoodCategoryViews

urlpatterns = []


router = DefaultRouter()
router.register(r'foods', FoodCategoryViews, basename='foods')



urlpatterns += router.urls