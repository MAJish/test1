from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import action

from body.models import Food, FoodCategory
from body.serializers import FoodSerializer, FoodListSerializer


class FoodCategoryViews(viewsets.ModelViewSet):
    serializer_class = FoodListSerializer
    #queryset = FoodCategory.objects.all()

    def get_queryset(self):
        result = []
        categories = FoodCategory.objects.all()
        for category in categories:
            food = Food.objects.filter(category=category, is_publish=True)
            if food:
                result.append(category)
        return result
